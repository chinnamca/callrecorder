package com.example.callrecorder.prime.indiacallrecorder.adapters;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.callrecorder.prime.indiacallrecorder.R;
import com.example.callrecorder.prime.indiacallrecorder.model.ModelFileGettingFromFolder;
import com.example.callrecorder.prime.indiacallrecorder.utils.AppConstant;
import com.example.callrecorder.prime.indiacallrecorder.utils.GPSTracker;
import com.example.callrecorder.prime.indiacallrecorder.utils.RecyclerViewClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;

import java.io.FileInputStream;
import java.util.List;

public class AdapterListOfFiles extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int viewTypeItem = 0;
    public static final int viewTypeLocation = 1;
    Context context;
    RecyclerViewClickListener clickingInterface;
    List<ModelFileGettingFromFolder> mDataset;
    boolean isLoading = false;
    boolean isMoreDataAvailable = true;
    GPSTracker gps;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    public AdapterListOfFiles(List<ModelFileGettingFromFolder> moviesList, Context cont) {
        this.mDataset = moviesList;
        this.context = cont;
        this.clickingInterface = (RecyclerViewClickListener) cont;
        MobileAds.initialize(cont, AppConstant.ADMOBAPI);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);


        return new MovieHolder(inflater.inflate(R.layout.listoffiles, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == viewTypeItem) {
          /*  if (mDataset.get(position).getType().equalsIgnoreCase(AppConstant.VIEWTYPENOADS)) {
                ((MovieHolder) holder).bindData(mDataset.get(position));
            } else if (mDataset.get(position).getType().equalsIgnoreCase(AppConstant.VIEWTYPEADS)) {
                ((MovieHolder) holder).bindAds();
            }*/
            ((MovieHolder) holder).bindData(mDataset.get(position));

        }
    }

    @Override
    public int getItemViewType(int position) {

        return mDataset.get(position) == null ? viewTypeLocation : viewTypeItem;
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        isLoading = false;
    }

    public void addItem(ModelFileGettingFromFolder dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {

        return mDataset == null ? 0 : mDataset.size();
    }

    public class MovieHolder extends RecyclerView.ViewHolder {
        TextView matchedUsertUsername;
        TextView matchedUserGender;
        CardView cardView;
        ImageView imgCallType;
        ImageView menuButton;
        NativeExpressAdView mAdView;
        VideoController mVideoController;

        public MovieHolder(View view) {
            super(view);

            matchedUsertUsername = (TextView) view.findViewById(R.id.matchedUsertUsername);
            matchedUserGender = (TextView) view.findViewById(R.id.matchedUserGender);
            imgCallType = (ImageView) view.findViewById(R.id.img_call_type);
            menuButton = (ImageView) view.findViewById(R.id.menuButton);
            cardView = (CardView) view.findViewById(R.id.card_view);
            mAdView = (NativeExpressAdView) view.findViewById(R.id.adViewProgress);
            mAdView.setVideoOptions(new VideoOptions.Builder()
                    .setStartMuted(true)
                    .build());
        }


        void bindData(final ModelFileGettingFromFolder user) {
            mAdView.setVisibility(View.GONE);
            if (user.getNoOfFile() % 12 == 0) {
                mAdView.setVisibility(View.GONE);
                AdRequest adRequest;
                gps = new GPSTracker(context);
                if (gps.canGetLocation()) {
                    adRequest = new AdRequest
                            .Builder()
                            .setLocation(gps.getLocation())
                            .build();
                } else {
                    adRequest = new AdRequest
                            .Builder()
                            .build();
                }
                mAdView.loadAd(adRequest);
                mVideoController = mAdView.getVideoController();
                mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                    @Override
                    public void onVideoEnd() {
                        super.onVideoEnd();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });
                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        mAdView.setVisibility(View.VISIBLE);
                        if (mVideoController.hasVideoContent()) {
                            //  Log.d("Log 1 ", "Received an ad that contains a video asset.");
                        } else {
                            //   Log.d("Log 1 ", "Received an ad that does not contain a video asset.");
                        }
                    }
                });
            }

            String incomingOrOutGoing = "";
            matchedUsertUsername.setText(user.getMobileNumber());
            if (user.getCallType() != null) {
                if (user.getCallType().equals("i")) {
                    incomingOrOutGoing = " Incoming call";
                    imgCallType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_leftarrow));
                    imgCallType.setColorFilter(ContextCompat.getColor(context, R.color.green));

                } else if (user.getCallType().equals("o")) {
                    incomingOrOutGoing = " Outgoing call";
                    imgCallType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_rightarrow));
                    imgCallType.setColorFilter(ContextCompat.getColor(context, R.color.red));

                } else {
                    incomingOrOutGoing = "";
                }
            } else {
                incomingOrOutGoing = "";
            }
            menuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickingInterface.onClick(getAdapterPosition());
                }
            });
            try {
                FileInputStream fis = new FileInputStream(user.getCallType());
                mediaPlayer.setDataSource(fis.getFD());
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.prepare();
            } catch (Exception e) {
                //e.printStackTrace();
            } finally {
                //System.out.println("Something went wrong");
            }
            matchedUserGender.setText(user.getDateOfCreated() + " " + user.getTimeOfCreated() + incomingOrOutGoing);
           /* if (user.getType().equalsIgnoreCase(AppConstant.VIEWTYPENOADS)) {

            } else if (user.getType().equalsIgnoreCase(AppConstant.VIEWTYPEADS)) {
                AdRequest adRequest;
                cardView.setVisibility(View.GONE);
                mAdView.setVisibility(View.GONE);
                gps = new GPSTracker(context);
                System.out.println("System location---> " + gps.canGetLocation());
                System.out.println("System location---> " + gps.getLocation());
                if (gps.canGetLocation()) {
                    adRequest = new AdRequest
                            .Builder()
                            .setLocation(gps.getLocation())
                            .build();
                } else {
                    adRequest = new AdRequest
                            .Builder()
                            .build();
                }

                mAdView.loadAd(adRequest);
                mVideoController = mAdView.getVideoController();
                mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                    @Override
                    public void onVideoEnd() {
                        super.onVideoEnd();
                        mAdView.setVisibility(View.VISIBLE);
                    }
                });

                mAdView.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {

                        mAdView.setVisibility(View.VISIBLE);
                        if (mVideoController.hasVideoContent()) {
                            //  Log.d("Log 1 ", "Received an ad that contains a video asset.");
                        } else {
                            //   Log.d("Log 1 ", "Received an ad that does not contain a video asset.");
                        }
                    }
                });
            }*/

        }

        void bindAds() {

        }
    }
}