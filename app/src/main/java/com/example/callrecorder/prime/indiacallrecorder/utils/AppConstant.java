package com.example.callrecorder.prime.indiacallrecorder.utils;

import android.os.Environment;

import java.util.ArrayList;

/**
 * Created by sathish on 29/06/17.
 */

public class AppConstant {

    public static final int FIRST_ROW = 0;
    public static final String ADMOBAPI = "ca-app-pub-7163841607518598~3414797418";
    public static final String PACKAGE = "com.example.callrecorder.prime.indiacallrecorder";
    //public static final String ADMOBAPI = "ca-app-pub-4256869031564558~8193093757";

    public static final int SECOND_ROW = 1;

    public static final int THIRD_ROW = 2;
    public static final String DELETE = "DELETE";
    public static final String CALL = "CALL";
    public static final String VIEWTYPEADS = "ADS";
    public static final String VIEWTYPENOADS = "NORMAL";

    public static ArrayList<String> filterList = new ArrayList<String>();

    public static String activityName = "WhiteList";
    public static String FOLDERPATH = Environment.getExternalStorageDirectory()
            .getAbsolutePath() + "/CallRecorder";

}
