package com.example.callrecorder.prime.indiacallrecorder.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.callrecorder.prime.indiacallrecorder.R;
import com.example.callrecorder.prime.indiacallrecorder.model.CallRecord;
import com.example.callrecorder.prime.indiacallrecorder.utils.AppConstant;
import com.example.callrecorder.prime.indiacallrecorder.utils.GPSTracker;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.sdsmdg.tastytoast.TastyToast;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class SaveDialogActivity extends Activity {

    ProgressDialog mProgress;
    LinearLayout layoutDelate, layoutSave, layoutSahre, layoutMessage, layoutCall, layoutPlay;
    AlertDialog.Builder loginDialog;
    NativeExpressAdView mAdView;
    VideoController mVideoController;
    Context mContext;
    Dialog chooseActionDialog;
    GPSTracker gps;
    private TextView contactName, mobilenumber, contactChar;
    private ImageView img_call_type, menuButton;
    private CallRecord callRecord;
    private SharedPreferences sharedPreferences, preferences;
    private SharedPreferences.Editor editor, editorID;
    private Boolean isUnknownNumber = false;
    private String EXISTING_FOLDER_ID;
    private AlertDialog dialog;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_save_dialog);
        mContext = this;
        MobileAds.initialize(this, AppConstant.ADMOBAPI);
        preferences = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editorID = preferences.edit();
        loginDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Dialog));
        EXISTING_FOLDER_ID = preferences.getString("EXISTING_FOLDER_ID", "");
        contactName = (TextView) findViewById(R.id.contact_name);
        mobilenumber = (TextView) findViewById(R.id.mobile_number);
        contactChar = (TextView) findViewById(R.id.contact_char);
        img_call_type = (ImageView) findViewById(R.id.img_call_type);
        menuButton = (ImageView) findViewById(R.id.menuButton);
        layoutDelate = (LinearLayout) findViewById(R.id.layoutDelate);
        layoutSave = (LinearLayout) findViewById(R.id.layoutSave);
        layoutSahre = (LinearLayout) findViewById(R.id.layoutSahre);
        layoutMessage = (LinearLayout) findViewById(R.id.layoutMessage);
        layoutCall = (LinearLayout) findViewById(R.id.layoutCall);
        layoutPlay = (LinearLayout) findViewById(R.id.layoutPlay);
        callRecord = new CallRecord();
        Bundle bundle = getIntent().getExtras();
        callRecord.setContactName(bundle.getString("contactName"));
        callRecord.setId(bundle.getLong("id"));
        callRecord.setFilePath(bundle.getString("filePath"));
        callRecord.setFileName(bundle.getString("fileName"));
        callRecord.setMobileNo(bundle.getString("mobileNumber"));
        callRecord.setTime(bundle.getString("date"));
        callRecord.setType(bundle.getString("type"));
        isUnknownNumber = bundle.getBoolean("unknownNumber");
        if (!isUnknownNumber) {
            layoutSave.setVisibility(View.GONE);
        } else {
            layoutSave.setVisibility(View.VISIBLE);
        }


        if (callRecord.getContactName() != null) {
            contactName.setText(callRecord.getContactName());

        } else {
            contactName.setText(getString(R.string.unknow));

        }
        if (callRecord.getMobileNo() != null) {
            mobilenumber.setText(callRecord.getMobileNo());

        } else {
            mobilenumber.setText(getString(R.string.unknow));

        }
        try {
            char c = callRecord.getContactName().charAt(0) == '+' ? 'U' : callRecord.getContactName().charAt(0);
            contactChar.setText(Character.toString(c));
            callRecord.setCategory("Personal");
            callRecord.setStorage("Local");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        sharedPreferences = getApplicationContext().getSharedPreferences("RecordSettings", 0); // 0 - for private mode
        editor = sharedPreferences.edit();

        layoutDelate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFileAndCall(AppConstant.DELETE);
            }
        });

        layoutSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveConatcts();
            }
        });


        layoutSahre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareToOthers();
            }
        });

        layoutMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessaeg();
            }
        });
        layoutCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFileAndCall(AppConstant.CALL);
            }
        });
        layoutPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent n = new Intent(SaveDialogActivity.this, AudioPlayer.class);
                n.putExtra("path", callRecord.getFilePath());

                if (!(callRecord.getContactName() == null)) {
                    n.putExtra("name", callRecord.getContactName());
                } else {
                    n.putExtra("name", callRecord.getMobileNo());
                }

                startActivity(n);
            }
        });


        mAdView = (NativeExpressAdView) findViewById(R.id.adViewProgress);

        adsShow();
    }

    public void deleteOlderFile() {
        File directory = new File(AppConstant.FOLDERPATH);
        File[] files = directory.listFiles();
        if (!(files == null)) {
            for (int i = 0; i <= files.length; i++) {
                if (files[i].exists()) {
                    Calendar time = Calendar.getInstance();
                    time.add(Calendar.DAY_OF_YEAR, -7);
                    Date lastModified = new Date(files[i].lastModified());
                    if (lastModified.before(time.getTime())) {
                    }
                    files[i].delete();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    public void shareToOthers() {
        File f = new File(callRecord.getFilePath());
        Uri uri = Uri.parse("file://" + f.getAbsolutePath());
        Intent share = new Intent(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.setType("audio/*");
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, "Share audio File"));
    }

    public void saveConatcts() {


        LayoutInflater factory = LayoutInflater.from(SaveDialogActivity.this);
        final View f = factory.inflate(R.layout.savecontacts, null);
        chooseActionDialog = new Dialog(SaveDialogActivity.this, R.style.CustomAlertDialog);
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);

        final EditText conatactName = (EditText) f.findViewById(R.id.conatactName);
        final Button saveToContact = (Button) f.findViewById(R.id.saveToContact);

        saveToContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(conatactName.getText().toString().length() == 0)) {
                    chooseActionDialog.dismiss();
                    addContact(conatactName.getText().toString(), callRecord.getMobileNo());
                } else {
                    conatactName.setError("Contact name empty");
                }

            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    public void sendMessaeg() {
        LayoutInflater factory = LayoutInflater.from(SaveDialogActivity.this);
        final View f = factory.inflate(R.layout.messagesend, null);
        chooseActionDialog = new Dialog(SaveDialogActivity.this, R.style.CustomAlertDialog);
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);
        final TextView messageTo = (TextView) f.findViewById(R.id.messageTo);
        final TextView messageCount = (TextView) f.findViewById(R.id.messageCount);
        final Button sendMessage = (Button) f.findViewById(R.id.sendMessage);
        final EditText messageText = (EditText) f.findViewById(R.id.messageText);
        if (!(callRecord.getContactName() == null)) {
            messageTo.setText(callRecord.getContactName());


        } else {
            messageTo.setText(callRecord.getMobileNo());
        }
        messageCount.setText("0/140");
        messageText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                messageCount.setText(messageText.getText().toString().length() + "/140");

                if (messageText.getText().toString().length() == 140) {
                    messageText.setError("You can only type 140 charecters");
                }
            }
        });
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!(messageText.getText().toString().length() == 0)) {

                    chooseActionDialog.dismiss();
                    sendMySMS(messageText.getText().toString(), callRecord.getMobileNo());
                } else {
                    messageText.setError("Message empty");
                }
            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    public void sendMySMS(String messageText, String phoneNumber) {


        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(messageText);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phoneNumber, null, msg, sentIntent, deliveredIntent);

        }

    }

    private void addContact(String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(Contacts.People.NUMBER, phone);
        values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        values.put(Contacts.People.LABEL, name);
        values.put(Contacts.People.NAME, name);
        Uri dataUri = getContentResolver().insert(Contacts.People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
        values.put(Contacts.People.NUMBER, phone);
        updateUri = getContentResolver().insert(updateUri, values);
    }

    public void deleteFileAndCall(final String type) {

        LayoutInflater factory = LayoutInflater.from(SaveDialogActivity.this);
        final View f = factory.inflate(R.layout.deleteselectedfile_layout, null);
        chooseActionDialog = new Dialog(SaveDialogActivity.this, R.style.CustomAlertDialog);
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);
        final Button sure = (Button) f.findViewById(R.id.sureButton);
        final Button no = (Button) f.findViewById(R.id.noButton);
        final TextView message = (TextView) f.findViewById(R.id.textView2);

        if (type.equalsIgnoreCase(AppConstant.DELETE)) {
            sure.setText(getString(R.string.delete));
            chooseActionDialog.setTitle(getString(R.string.delete));
            message.setText(getString(R.string.confirmdelete));

        } else {
            sure.setText(getString(R.string.call));
            chooseActionDialog.setTitle(getString(R.string.call));
            if (!(callRecord.getContactName() == null)) {
                message.setText(R.string.confirmcall + " to " + callRecord.getContactName());
            } else {
                message.setText(R.string.confirmcall + " to " + callRecord.getMobileNo());
            }

        }
        sure.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                if (type.equalsIgnoreCase(AppConstant.DELETE)) {
                    File file = new File(callRecord.getFilePath());
                    if (file.exists()) {
                        if (file.delete()) {
                            TastyToast.makeText(getApplicationContext(), getString(R.string.filedeleted), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                            finish();
                        } else {
                            TastyToast.makeText(getApplicationContext(), getString(R.string.settingmessage),
                                    TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callRecord.getMobileNo()));
                    startActivity(intent);
                    if (!(callRecord.getContactName() == null)) {
                        TastyToast.makeText(getApplicationContext(), getString(R.string.calling) + " to " +
                                callRecord.getContactName(), TastyToast.LENGTH_LONG, TastyToast.INFO);
                        finish();

                    } else {
                        TastyToast.makeText(getApplicationContext(), getString(R.string.calling) + " to " + callRecord.getMobileNo()
                                , TastyToast.LENGTH_LONG, TastyToast.INFO);
                    }


                }


            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sentStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:

                        TastyToast.makeText(getApplicationContext(), getString(R.string.msgsuccessfully), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.genericerror), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.noservice), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.nullpdu), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.rediooff), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    default:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.unknownerror), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                }


            }
        };
        deliveredStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.deleivered), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        break;
                    case Activity.RESULT_CANCELED:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.nodelevery), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        break;
                }
            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);

    }

    public void adsShow() {
        gps = new GPSTracker(mContext);
        AdRequest adRequest = null;
        if (gps.canGetLocation()) {
            adRequest = new AdRequest
                    .Builder()
                    .setLocation(gps.getLocation())
                    .build();
        } else {
            adRequest = new AdRequest
                    .Builder()
                    .build();
        }
        mAdView.setVisibility(View.GONE);
        mAdView.setVideoOptions(new VideoOptions.Builder()
                .setStartMuted(true)
                .build());
        mAdView.loadAd(adRequest);
        mVideoController = mAdView.getVideoController();
        mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            @Override
            public void onVideoEnd() {
                super.onVideoEnd();
            }
        });
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mAdView.setVisibility(View.VISIBLE);
                if (mVideoController.hasVideoContent()) {
                } else {
                }
            }
        });

    }

}
