package com.example.callrecorder.prime.indiacallrecorder.utils;

/**
 * Created by pattronize on 20/11/17.
 */

public interface RecyclerViewClickListener {

    void onClick(int position);
}
