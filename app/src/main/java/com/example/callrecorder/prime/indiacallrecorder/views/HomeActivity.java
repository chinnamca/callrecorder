package com.example.callrecorder.prime.indiacallrecorder.views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dgreenhalgh.android.simpleitemdecoration.linear.EndOffsetItemDecoration;
import com.example.callrecorder.prime.indiacallrecorder.R;
import com.example.callrecorder.prime.indiacallrecorder.adapters.AdapterListOfFiles;
import com.example.callrecorder.prime.indiacallrecorder.model.ModelFileGettingFromFolder;
import com.example.callrecorder.prime.indiacallrecorder.utils.AppConstant;
import com.example.callrecorder.prime.indiacallrecorder.utils.GPSTracker;
import com.example.callrecorder.prime.indiacallrecorder.utils.RecyclerViewClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.sdsmdg.tastytoast.TastyToast;
import com.shawnlin.numberpicker.NumberPicker;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;
import pub.devrel.easypermissions.EasyPermissions;

public class HomeActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, RecyclerViewClickListener {
    private static final int REQUEST_SMS = 0;
    private static String LOG_TAG = "EXAMPLE";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ArrayList<ModelFileGettingFromFolder> listOfRecordAudio = new ArrayList<>();
    AdapterListOfFiles adapterListOfFiles;
    RecyclerView.LayoutManager mLayoutManager;
    Context mContext;
    AlertDialog.Builder loginDialog;
    Dialog chooseActionDialog;
    boolean doubleBackToExitPressedOnce = false;
    TextView textviewToolbar;
    GPSTracker gps;
    NativeExpressAdView mAdView;
    VideoController mVideoController;
    RelativeLayout recyclerViewLayouts;
    ProgressBar progressBar;
    private String EXISTING_FOLDER_ID;
    private RecyclerView recyclerView;
    private TextView noRecordWarning;
    private AlertDialog dialog;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;
    private MediaPlayer mediaPlayer = new MediaPlayer();

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);
        mContext = HomeActivity.this;

        MultiDex.install(this);
        MobileAds.initialize(mContext, "ca-app-pub-4256869031564558~8193093757");
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        editor = pref.edit();
        editor.putString("FOLDER_PATH", AppConstant.FOLDERPATH);
        editor.putBoolean("callRecord", true);
        editor.putBoolean("SETDAYS", false);
        editor.putInt("UN_READ_CALLS", 1);
        editor.putInt("UN_READ_CALLS", 0);
        gps = new GPSTracker(mContext);
        recyclerViewLayouts = (RelativeLayout) findViewById(R.id.recyclerViewLayouts);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerViewLayouts.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        ShortcutBadger.removeCount(getApplicationContext()); //for 1.1.4+
        if (!pref.getBoolean("SETDAYS", false)) {
            editor.putInt("DAYS_AFTER_DELETE", 7);
            editor.putBoolean("SETDAYS", true);
        } else {

        }
        editor.commit();
        chooseActionDialog = new Dialog(HomeActivity.this, R.style.CustomAlertDialog);
        loginDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Dialog));
        EXISTING_FOLDER_ID = pref.getString("EXISTING_FOLDER_ID", "");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            System.out.println("--------");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textviewToolbar = (TextView) findViewById(R.id.textviewToolbar);
        textviewToolbar.setText(getString(R.string.app_name));
        noRecordWarning = (TextView) findViewById(R.id.text_view);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setVisibility(View.GONE);
        File directory = new File(AppConstant.FOLDERPATH);
        File[] files = directory.listFiles();
        if (!(files == null)) {
            fileAddToRecyclerView();

        } else {
            recyclerViewLayouts.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            noRecordWarning.setVisibility(View.VISIBLE);
        }
        int offsetPx = 10;
        recyclerView.addItemDecoration(new EndOffsetItemDecoration(offsetPx));
    }

    public String getContactName(final String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName = "";
        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(0);
            }
            cursor.close();
        }
        return contactName;
    }

    public void fileAddToRecyclerView() {
        progressBar.setVisibility(View.VISIBLE);
        File directory = new File(AppConstant.FOLDERPATH);
        File[] files = directory.listFiles();
        if (!(files == null)) {


            for (int i = 0; i < files.length; i++) {
                ModelFileGettingFromFolder modelFileGettingFromFolder = new ModelFileGettingFromFolder();
                /*if (i % 5 == 0) {
                    modelFileGettingFromFolder.setType(AppConstant.VIEWTYPEADS);
                } else {
                    modelFileGettingFromFolder.setType(AppConstant.VIEWTYPENOADS);
                }*/
                modelFileGettingFromFolder.setNoOfFile(i);
                modelFileGettingFromFolder.setFilePath(String.valueOf(files[i]));
                String[] items = files[i].getName().split("_");
                for (int j = 0; j < items.length; j++) {

                    if (j == 1) {
                        if (!(getContactName(items[1]).equalsIgnoreCase(""))) {
                            modelFileGettingFromFolder.setMobileNumber(getContactName(items[1]));
                        } else {
                            modelFileGettingFromFolder.setMobileNumber(items[1]);
                        }
                    }
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date convertedDate = new Date();
                    try {
                        convertedDate = dateFormat.parse(items[0]);
                        modelFileGettingFromFolder.setDate(convertedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (j == 0) {
                        String[] itemDate = items[0].split("\\ ");
                        for (int k = 0; k < itemDate.length; k++) {
                            modelFileGettingFromFolder.setDateOfCreated("" + itemDate[0]);
                            modelFileGettingFromFolder.setTimeOfCreated("" + itemDate[1]);
                        }
                    }
                    if (j == 2) {
                        String[] itemCallType = items[2].split("\\.");
                        for (int k = 0; k < itemCallType.length; k++) {
                            modelFileGettingFromFolder.setCallType("" + itemCallType[0]);
                        }
                    }
                }
                listOfRecordAudio.add(modelFileGettingFromFolder);
            }


            Collections.sort(listOfRecordAudio, new Comparator<ModelFileGettingFromFolder>() {
                public int compare(ModelFileGettingFromFolder s1, ModelFileGettingFromFolder s2) {
                    return (s2.getDate()).compareTo(s1.getDate());
                }
            });
            progressBar.setVisibility(View.GONE);
            recyclerViewLayouts.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            noRecordWarning.setVisibility(View.GONE);
            adapterListOfFiles = new AdapterListOfFiles(listOfRecordAudio, mContext);
            recyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapterListOfFiles);
            adapterListOfFiles.notifyDataSetChanged();

        } else {
            TastyToast.makeText(getApplicationContext(), getString(R.string.norecord), TastyToast.LENGTH_LONG, TastyToast.INFO);
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            TastyToast.makeText(getApplicationContext(), getString(R.string.exit), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);

        }
        if (!doubleBackToExitPressedOnce) {
            TastyToast.makeText(getApplicationContext(), getString(R.string.clickagain), TastyToast.LENGTH_LONG, TastyToast.INFO);
        }
        doubleBackToExitPressedOnce = true;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);


    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppConstant.filterList.clear();

    }

    @Override
    protected void onResume() {
        super.onResume();
        listOfRecordAudio.clear();
        listOfRecordAudio.clear();
        fileAddToRecyclerView();
        sentStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:

                        TastyToast.makeText(getApplicationContext(), getString(R.string.msgsuccessfully), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.genericerror), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.noservice), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.nullpdu), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.rediooff), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                    default:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.unknownerror), TastyToast.LENGTH_LONG, TastyToast.CONFUSING);

                        break;
                }


            }
        };
        deliveredStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.deleivered), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        break;
                    case Activity.RESULT_CANCELED:
                        TastyToast.makeText(getApplicationContext(), getString(R.string.nodelevery), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        break;
                }
            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        listOfRecordAudio.clear();
        fileAddToRecyclerView();
        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);

    }

    public void sendMessaeg(final int position) {

        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
        final View f = factory.inflate(R.layout.messagesend, null);
        //chooseActionDialog = new Dialog( HomeActivity.this, R.style.CustomAlertDialogWithbackgroun );

        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);
        final TextView messageTo = (TextView) f.findViewById(R.id.messageTo);
        final TextView messageCount = (TextView) f.findViewById(R.id.messageCount);
        final Button sendMessage = (Button) f.findViewById(R.id.sendMessage);
        final EditText messageText = (EditText) f.findViewById(R.id.messageText);
        messageTo.setText(listOfRecordAudio.get(position).getMobileNumber());
        messageCount.setText("0/140");
        messageText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                messageCount.setText(messageText.getText().toString().length() + "/140");

                if (messageText.getText().toString().length() == 140) {
                    messageText.setError("You can only type 140 charecters");
                }
            }
        });

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!(messageText.getText().toString().length() == 0)) {

                    chooseActionDialog.dismiss();
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
                        if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                            if (!shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                                showMessageOKCancel("You need to allow access to Send SMS",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                                                            REQUEST_SMS);
                                                }
                                            }
                                        });
                                return;
                            }
                            requestPermissions(new String[]{Manifest.permission.SEND_SMS},
                                    REQUEST_SMS);
                            return;
                        }

                        if (getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()).equalsIgnoreCase("Unsaved")) {

                            sendMySMS(messageText.getText().toString(), listOfRecordAudio.get(position).getMobileNumber());

                        } else {

                            sendMySMS(messageText.getText().toString(), getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()));
                        }
                    }
                } else {
                    messageText.setError("Message empty");
                }

            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    private void addContact(String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(Contacts.People.NUMBER, phone);
        values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        values.put(Contacts.People.LABEL, name);
        values.put(Contacts.People.NAME, name);
        Uri dataUri = getContentResolver().insert(Contacts.People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
        values.put(Contacts.People.NUMBER, phone);
        updateUri = getContentResolver().insert(updateUri, values);
    }

    public void saveConatcts(final int position) {


        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
        final View f = factory.inflate(R.layout.savecontacts, null);
        //chooseActionDialog = new Dialog( HomeActivity.this, R.style.CustomAlertDialogWithbackgroun );
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);

        final EditText conatactName = (EditText) f.findViewById(R.id.conatactName);
        final Button saveToContact = (Button) f.findViewById(R.id.saveToContact);

        saveToContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(conatactName.getText().toString().length() == 0)) {
                    chooseActionDialog.dismiss();
                    addContact(conatactName.getText().toString(), listOfRecordAudio.get(position).getMobileNumber());
                } else {
                    conatactName.setError("Contact name empty");
                }

            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    public void sendMySMS(String messageText, String phoneNumber) {

        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(messageText);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phoneNumber, null, msg, sentIntent, deliveredIntent);

        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void choosedAction(final int position) {

        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
        final View f = factory.inflate(R.layout.chooseaction, null);

        chooseActionDialog.setContentView(f);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);

        final Button share = (Button) f.findViewById(R.id.share);
        final Button call = (Button) f.findViewById(R.id.call);
        final Button message = (Button) f.findViewById(R.id.message);
        final Button delete = (Button) f.findViewById(R.id.delete);
        final Button play = (Button) f.findViewById(R.id.play);
        final Button saveToContact = (Button) f.findViewById(R.id.saveToContact);


        if (getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()).equalsIgnoreCase("Unsaved")) {
            saveToContact.setVisibility(View.VISIBLE);
        } else {
            saveToContact.setVisibility(View.GONE);

        }
        saveToContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                saveConatcts(position);
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                shareToOthers(position);
            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                Intent n = new Intent(HomeActivity.this, AudioPlayer.class);
                n.putExtra("path", listOfRecordAudio.get(position).getFilePath());
                n.putExtra("name", listOfRecordAudio.get(position).getMobileNumber());
                startActivity(n);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                deleteFileAndCall(position, AppConstant.CALL);

            }
        });
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                sendMessaeg(position);
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                deleteFileAndCall(position, AppConstant.DELETE);
            }
        });
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();

        chooseActionDialog.show();
    }

    public void deleteFileAndCall(final int position, final String type) {

        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
        final View f = factory.inflate(R.layout.deleteselectedfile_layout, null);
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);
        final Button sure = (Button) f.findViewById(R.id.sureButton);
        final Button no = (Button) f.findViewById(R.id.noButton);
        final TextView message = (TextView) f.findViewById(R.id.textView2);

        if (type.equalsIgnoreCase(AppConstant.DELETE)) {
            message.setText("");
            sure.setText(getString(R.string.delete));
            chooseActionDialog.setTitle(getString(R.string.delete));
            message.setText(getString(R.string.confirmdelete));

        } else {
            sure.setText(getString(R.string.call));
            chooseActionDialog.setTitle(getString(R.string.call));

            if (getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()).equalsIgnoreCase("Unsaved")) {
                message.setText("");
                message.setText(getString(R.string.confirmcall) + " " + listOfRecordAudio.get(position)
                        .getMobileNumber());
            } else {
                message.setText("");
                message.setText(getString(R.string.confirmcall) + " " + getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()));

            }

        }
        sure.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
                if (type.equalsIgnoreCase(AppConstant.DELETE)) {
                    File fdelete = new File(listOfRecordAudio.get(position).getFilePath());
                    if (fdelete.exists()) {
                        if (fdelete.delete()) {
                            adapterListOfFiles.notifyItemRemoved(position);
                            listOfRecordAudio.remove(position);
                            adapterListOfFiles.notifyDataSetChanged();
                            TastyToast.makeText(getApplicationContext(), getString(R.string.filedeleted), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        } else {
                            TastyToast.makeText(getApplicationContext(), getString(R.string.somethingwrong), TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                    }
                } else {


                    if (getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()).equalsIgnoreCase("Unsaved")) {


                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + listOfRecordAudio.get(position).getMobileNumber()));
                        startActivity(intent);
                        TastyToast.makeText(getApplicationContext(), getString(R.string.calling) + " to " + listOfRecordAudio.get(position)
                                .getMobileNumber(), TastyToast.LENGTH_LONG, TastyToast.INFO);

                    } else {

                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +
                                getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber())));
                        startActivity(intent);
                        TastyToast.makeText(getApplicationContext(), getString(R.string.calling) + " to " +
                                getPhoneNumber(listOfRecordAudio.get(position).getMobileNumber()), TastyToast.LENGTH_LONG, TastyToast.INFO);
                    }
                }


            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseActionDialog.dismiss();
            }
        });

        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    @Override
    public void onClick(int position) {
        choosedAction(position);


    }

    public String getPhoneNumber(String name) {
        String ret = null;
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " like'%" + name + "%'";
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, selection, null, null);
        if (c.moveToFirst()) {
            ret = c.getString(0);
        }
        c.close();
        if (ret == null)
            ret = "Unsaved";
        return ret;
    }

    public void shareToOthers(final int position) {
        File f = new File(listOfRecordAudio.get(position).getFilePath());
        Uri uri = Uri.parse("file://" + f.getAbsolutePath());
        Intent share = new Intent(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.setType("audio/*");
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, "Share audio File"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                deleteFileDates();
                return true;
            case R.id.whitelist:
                Intent n = new Intent(HomeActivity.this, WhiteListActivity.class);
                startActivity(n);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void deleteFileDates() {


        LayoutInflater factory = LayoutInflater.from(HomeActivity.this);
        final View f = factory.inflate(R.layout.file_delete_n_number_of_days, null);
        chooseActionDialog = new Dialog(HomeActivity.this, R.style.CustomAlertDialog);
        chooseActionDialog.setContentView(f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(chooseActionDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        chooseActionDialog.getWindow().setAttributes(lp);
        chooseActionDialog.setCancelable(true);
        chooseActionDialog.setCanceledOnTouchOutside(true);

        final TextView message = (TextView) f.findViewById(R.id.textView2);//textView2

        final NumberPicker numberPicker = (NumberPicker) f.findViewById(R.id.number_picker);
        numberPicker.setOrientation(LinearLayout.VERTICAL);
        numberPicker.setValue(pref.getInt("DAYS_AFTER_DELETE", 7));

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                System.out.println("scrool vlaues--> " + newVal);
                editor.putInt("DAYS_AFTER_DELETE", newVal);
                editor.commit();
                message.setText("App will delete the last " + pref.getInt("DAYS_AFTER_DELETE", 7) + " days files from device automatically  ");
            }
        });
        message.setText("App will delete the last " + pref.getInt("DAYS_AFTER_DELETE", 7) + " days files from device automatically  ");
        mAdView = (NativeExpressAdView) f.findViewById(R.id.adViewProgress);
        adsShow();
        chooseActionDialog.show();
    }

    public void adsShow() {
        mAdView.setVisibility(View.GONE);
        AdRequest adRequest = null;
        if (gps.canGetLocation()) {
            adRequest = new AdRequest
                    .Builder()
                    .setLocation(gps.getLocation())
                    .build();
        } else {
            adRequest = new AdRequest
                    .Builder()
                    .build();

        }

        mAdView.setVideoOptions(new VideoOptions.Builder()
                .setStartMuted(true)
                .build());
        mAdView.loadAd(adRequest);
        mVideoController = mAdView.getVideoController();
        mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            @Override
            public void onVideoEnd() {
                Log.d("Save Activity ", "Video playback is finished.");
                super.onVideoEnd();
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                mAdView.setVisibility(View.VISIBLE);
                if (mVideoController.hasVideoContent()) {
                    Log.d("Save Activity ", "Received an ad that contains a video asset.");
                } else {
                    Log.d("Save Activity ", "Received an ad that does not contain a video asset.");
                }
            }
        });
    }
}


