package com.example.callrecorder.prime.indiacallrecorder.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.example.callrecorder.prime.indiacallrecorder.R;
import com.example.callrecorder.prime.indiacallrecorder.services.CallRecorderService;
import com.example.callrecorder.prime.indiacallrecorder.utils.AppConstant;
import com.example.callrecorder.prime.indiacallrecorder.utils.UtilPermissions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.SEND_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashScreen extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final int PERMISSION_ALL = 0;
    Context context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String[] PERMISSIONS = {
            READ_PHONE_STATE,
            WRITE_EXTERNAL_STORAGE,
            RECORD_AUDIO,
            READ_CONTACTS,
            CALL_PHONE,
            SEND_SMS,
            ACCESS_FINE_LOCATION,
            READ_CONTACTS
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = this;
        if (!UtilPermissions.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            Intent i = new Intent(CallRecorderService.ACTION);
            i.setPackage(AppConstant.PACKAGE);
            startService(i);
            pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            editor = pref.edit();
            editor.putString("EXISTING_FOLDER_ID", null);
            editor.commit();
            startActivity(new Intent(SplashScreen.this, HomeActivity.class));
            finish();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        int index = 0;
        Map<String, Integer> PermissionsMap = new HashMap<String, Integer>();
        for (String permission : permissions) {
            PermissionsMap.put(permission, grantResults[index]);
            index++;
        }

        if ((PermissionsMap.get(READ_PHONE_STATE) != 0) || (PermissionsMap.get(WRITE_EXTERNAL_STORAGE) != 0)
                || ((PermissionsMap.get(RECORD_AUDIO)) != 0) || (PermissionsMap.get(READ_CONTACTS) != 0)
                || (PermissionsMap.get(CALL_PHONE) != 0) || (PermissionsMap.get(SEND_SMS) != 0
                || (PermissionsMap.get(READ_CONTACTS) != 0) || (PermissionsMap.get(ACCESS_FINE_LOCATION) != 0))) {
            //Toast.makeText(this, "Please allow all the permissions", Toast.LENGTH_SHORT).show();

            // promptSettings();

            if (!UtilPermissions.hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            } else {
                Intent i = new Intent(CallRecorderService.ACTION);
                i.setPackage(AppConstant.PACKAGE);
                startService(i);
                pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                editor = pref.edit();
                editor.putString("EXISTING_FOLDER_ID", null);
                editor.commit();
                startActivity(new Intent(SplashScreen.this, HomeActivity.class));
                finish();
            }


        } else {
            pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            editor = pref.edit();
            editor.putString("EXISTING_FOLDER_ID", null);
            editor.commit();
            startActivity(new Intent(SplashScreen.this, HomeActivity.class));
            finish();

        }
    }

    private void promptSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.unabletoopen));
        builder.setMessage(getString(R.string.settingmessage));
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                goToSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                System.exit(0);
            }
        });
        builder.show();
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + this.getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(myAppSettings);
        finish();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.

    }


    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

}