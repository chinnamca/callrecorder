package com.example.callrecorder.prime.indiacallrecorder.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pattronize on 16/11/17.
 */

public class ModelFileGettingFromFolder implements Serializable {

    String mobileNumber;
    String dateOfCreated;
    String timeOfCreated;
    String noon;
    String callType;
    String filePath;
    String type;
    int noOfFile;
    Date date;

    public int getNoOfFile() {
        return noOfFile;
    }

    public void setNoOfFile(int noOfFile) {
        this.noOfFile = noOfFile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDateOfCreated() {
        return dateOfCreated;
    }

    public void setDateOfCreated(String dateOfCreated) {
        this.dateOfCreated = dateOfCreated;
    }

    public String getTimeOfCreated() {
        return timeOfCreated;
    }

    public void setTimeOfCreated(String timeOfCreated) {
        this.timeOfCreated = timeOfCreated;
    }

    public String getNoon() {
        return noon;
    }

    public void setNoon(String noon) {
        this.noon = noon;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
