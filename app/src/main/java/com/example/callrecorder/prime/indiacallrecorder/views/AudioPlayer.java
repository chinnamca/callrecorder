package com.example.callrecorder.prime.indiacallrecorder.views;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.callrecorder.prime.indiacallrecorder.R;
import com.example.callrecorder.prime.indiacallrecorder.utils.AppConstant;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;

public class AudioPlayer extends Activity {
    public static int oneTimeOnly = 0;
    NativeExpressAdView mAdView;
    VideoController mVideoController;
    private ImageView playPauseImg;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private double startTime = 0;
    private Handler durationHandler = new Handler();
    private double timeElapsed = 0, finalTime = 0;
    private SeekBar seekbar;
    private TextView startTimeView, stopTimeView, fileName;
    private String path;
    private Button share, cancel;
    private Runnable UpdateSongTime = new Runnable() {
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
            startTimeView.setText(String.format("%d:%d",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekbar.setProgress((int) startTime);
            durationHandler.postDelayed(this, 100);
            if (!mediaPlayer.isPlaying()) {
                playPauseImg.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.play_img));
            }
        }
    };
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_audio_player);
//        MobileAds.initialize(this, "ca-app-pub-4256869031564558~8193093757");
        MobileAds.initialize(this, AppConstant.ADMOBAPI);

        playPauseImg = (ImageView) findViewById(R.id.play_pause);

        startTimeView = (TextView) findViewById(R.id.textView2);
        stopTimeView = (TextView) findViewById(R.id.textView3);
        fileName = (TextView) findViewById(R.id.file_name);
        share = (Button) findViewById(R.id.share);
        cancel = (Button) findViewById(R.id.cancel);

        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("path");
        fileName.setText(bundle.getString("name"));

        seekbar = (SeekBar) findViewById(R.id.seekBar);
        playPauseImg.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.pause_img));

        try {
            FileInputStream fis = new FileInputStream(path);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "File not found", Toast.LENGTH_SHORT).show();
        }

        finalTime = mediaPlayer.getDuration();
        startTime = mediaPlayer.getCurrentPosition();

        if (oneTimeOnly == 0) {
            seekbar.setMax((int) finalTime);
            oneTimeOnly = 1;
        }

        stopTimeView.setText(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                finalTime)))
        );
        startTimeView.setText(String.format("%d:%d",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                startTime)))
        );
        if (!mediaPlayer.isPlaying()) {
            playAudio();

        }
        if (mediaPlayer != null) {
            int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
            seekbar.setProgress(mCurrentPosition);
        }

        playPauseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    playPauseImg.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.play_img));
                } else {
                    playAudio();
                    playPauseImg.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.pause_img));
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneTimeOnly = 0;
                mediaPlayer.stop();
                File f = new File(path);
                Uri uri = Uri.parse("file://" + f.getAbsolutePath());
                Intent share = new Intent(Intent.ACTION_SEND);
                share.putExtra(Intent.EXTRA_STREAM, uri);
                share.setType("audio/*");
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(share, "Share audio File"));
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneTimeOnly = 0;
                mediaPlayer.stop();
                finish();


            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    //mediaPlayer.seekTo(progress * 1000);
                    mediaPlayer.seekTo(seekBar.getProgress());

                }
            }
        });


        mAdView = (NativeExpressAdView) findViewById(R.id.adViewProgress);
        mAdView.setVisibility(View.GONE);
        mAdView.setVideoOptions(new VideoOptions.Builder()
                .setStartMuted(true)
                .build());
        mAdView.loadAd(new AdRequest.Builder().build());
        mVideoController = mAdView.getVideoController();
        mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
            @Override
            public void onVideoEnd() {
                Log.d("Save Activity ", "Video playback is finished.");
                super.onVideoEnd();
            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                mAdView.setVisibility(View.VISIBLE);
                if (mVideoController.hasVideoContent()) {
                    Log.d("Save Activity ", "Received an ad that contains a video asset.");
                } else {
                    Log.d("Save Activity ", "Received an ad that does not contain a video asset.");
                }
            }
        });
    }

    private void playAudio() {
        mediaPlayer.start();
        timeElapsed = mediaPlayer.getCurrentPosition();
        seekbar.setProgress((int) timeElapsed);
        durationHandler.postDelayed(UpdateSongTime, 100);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        oneTimeOnly = 0;
        mediaPlayer.stop();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        oneTimeOnly = 0;
        mediaPlayer.stop();
    }
}











